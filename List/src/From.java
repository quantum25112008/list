

public class From {

 private String accountNumber;

 public From() {
 }

 public String getAccountNumber() {
  return accountNumber;
 }

 public void setAccountNumber(String accountNumber) {
  this.accountNumber = accountNumber;
 }

 @Override
 public String toString() {
  return "From{" +
          "accountNumber='" + accountNumber + '\'' +
          '}';
 }
}