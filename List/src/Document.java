public class Document {
    private String type;
    private String serial;
    private String number;
    private String date;
    private String organization;
    private String division;
    private String expireDate;

    public Document() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    @Override
    public String toString() {
        return "Document{" +
                "type='" + type + '\'' +
                ", serial='" + serial + '\'' +
                ", number='" + number + '\'' +
                ", date='" + date + '\'' +
                ", organization='" + organization + '\'' +
                ", division='" + division + '\'' +
                ", expireDate='" + expireDate + '\'' +
                '}';
    }
}
