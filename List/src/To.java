

public class To {

    private String name;

    private String inn;

    private String kpp;

    private String bik;

    private String bankName;

    private String corrAccountNumber;

    private String accountNumber;


    public To() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getKpp() {
        return kpp;
    }

    public void setKpp(String kpp) {
        this.kpp = kpp;
    }

    public String getBik() {
        return bik;
    }

    public void setBik(String bik) {
        this.bik = bik;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getCorrAccountNumber() {
        return corrAccountNumber;
    }

    public void setCorrAccountNumber(String corrAccountNumber) {
        this.corrAccountNumber = corrAccountNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Override
    public String toString() {
        return "To{" +
                "name='" + name + '\'' +
                ", inn='" + inn + '\'' +
                ", kpp='" + kpp + '\'' +
                ", bik='" + bik + '\'' +
                ", bankName='" + bankName + '\'' +
                ", corrAccountNumber='" + corrAccountNumber + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                '}';
    }
}