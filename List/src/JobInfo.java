public class JobInfo {
    private String position;

    public JobInfo() {
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "JobInfo{" +
                "position='" + position + '\'' +
                '}';
    }
}
