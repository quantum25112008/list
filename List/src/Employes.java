import java.util.List;

public class Employes {

    private Integer number;

    private String firtname;

    private String lastname;

    private String middleName;

    private String birthDate;

    private String birthPlace;

    private String citizenship;

    private String email;

    private String latinFirstName;

    private String latinLastName;

    private List<Phones> phones;

  //  private List<Documents> employes;
    private List<Document> document;


    public Employes() {
    }


    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getFirtname() {
        return firtname;
    }

    public void setFirtname(String firtname) {
        this.firtname = firtname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLatinFirstName() {
        return latinFirstName;
    }

    public void setLatinFirstName(String latinFirstName) {
        this.latinFirstName = latinFirstName;
    }

    public String getLatinLastName() {
        return latinLastName;
    }

    public void setLatinLastName(String latinLastName) {
        this.latinLastName = latinLastName;
    }

    public List<Phones> getPhones() {
        return phones;
    }

    public void setPhones(List<Phones> phones) {
        this.phones = phones;
    }

    public List<Document> getDocument() {
        return document;
    }

    public void setDocument(List<Document> document) {
        this.document = document;
    }

    @Override
    public String toString() {
        return "Employes{" +
                "number=" + number +
                ", firtname='" + firtname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", middleName='" + middleName + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", birthPlace='" + birthPlace + '\'' +
                ", citizenship='" + citizenship + '\'' +
                ", email='" + email + '\'' +
                ", latinFirstName='" + latinFirstName + '\'' +
                ", latinLastName='" + latinLastName + '\'' +
                ", phones=" + phones +
                ", document=" + document +
                '}';
    }
}
