

public class Meta {

    private String clientCustomField;

    public Meta() {
    }

    public String getClientCustomField() {
        return clientCustomField;
    }

    public void setClientCustomField(String clientCustomField) {
        this.clientCustomField = clientCustomField;
    }

    @Override
    public String toString() {
        return "Meta{" +
                "clientCustomField='" + clientCustomField + '\'' +
                '}';
    }
}