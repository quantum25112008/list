import java.util.List;

public class Drafts {

    private String correlationId;

    private List<Employes> employes;


    public Drafts() {
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public List<Employes> getEmployes() {
        return employes;
    }

    public void setEmployes(List<Employes> employes) {
        this.employes = employes;
    }

    @Override
    public String toString() {
        return "Drafts{" +
                "correlationId='" + correlationId + '\'' +
                ", employes=" + employes +
                '}';
    }
}
